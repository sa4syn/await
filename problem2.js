const fs = require('fs')
const readline = require('readline');

function promiseToReadFile(filePath) {
    return new Promise((resolve, reject) => {
        fs.readFile(filePath, 'utf-8', (err, fileContent) => {
            if (err) {
                reject('No such file!' + filePath);
            } else {
                resolve(fileContent);
            }
        })
    })
}

function promiseToWriteFile(filePath, content) {
    return new Promise((resolve, reject) => {
        fs.writeFile(filePath, content, 'utf-8', (err) => {
            if (err) {
                reject();
            } else {
                resolve('file Created');
            }
        })
    })
}

async function readProcessWriteAppendName(readFrom, writeTo, appendTo, processText = (content) => content) {
    let fileContent = await promiseToReadFile(readFrom);
    await promiseToWriteFile(writeTo, processText(fileContent));
    appendTo.write(writeTo + '\n');
}

function promiseToDeleteFile(filePath) {
    return new Promise((resolve, reject) => {
        fs.unlink(filePath, (err) => {
            if (err) {
                console.log(err);
                reject('No such file')
            } else {
                resolve();
            }
        })
    })
}

async function processLipsum(lipsumFile) {
    let upperCaseFile = './upperCase.txt';
    let lowerCaseFile = './lowerCase.txt';
    let sortContents = './sortFile.txt';
    const fileNamesStream = fs.createWriteStream('filenames.txt');
    await readProcessWriteAppendName(lipsumFile, upperCaseFile, fileNamesStream, (content) => content.toUpperCase());
    await readProcessWriteAppendName(upperCaseFile, lowerCaseFile, fileNamesStream, (content) => content.toLowerCase().split('.').join(''));
    await readProcessWriteAppendName(lowerCaseFile, sortContents, fileNamesStream, (content) => content.split('.').sort().join(''));
    fileNamesStream.end();
    const file = readline.createInterface({
        input: fs.createReadStream('filenames.txt'),
        output: process.stdout,
        terminal: false
    });
    file.on('line', (line) => {
        console.log(line, "works");
        promiseToDeleteFile(line);
    });
}

module.exports = { processLipsum };