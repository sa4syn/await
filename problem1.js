const fs = require('fs');
const { resolve } = require('path');

function getRandomStringArray(arrayLength, wordLength = 4) {
    let outputArray = new Array(arrayLength).fill('');
    outputArray.forEach((randomString, index, array) => {
        array[index] = Math.random().toString(36).substring(2, wordLength);
    });
    return outputArray;
};

function promiseToCreatFile(fileName){
    return  new Promise((resolve, reject) => {
        fs.writeFile(fileName, '', 'utf-8', function(err){
            if(err){
                reject(err);
            } else {
                console.log('created ' + fileName);
                resolve();
            }
        })
    })
}

function promiseToDeleteFile(fileName){
    return  new Promise((resolve, reject) => {
        fs.unlink(fileName, function(err) {
            if(err){
                console.log(err);
                reject(err);
            } else {
                console.log('deleted ' + fileName);
                resolve();
            }
        })
    })
}

async function awaitDelete(fileName){
    await promiseToCreatFile(fileName);
    promiseToDeleteFile(fileName)
}

async function createAndDeleteRandomJson(numberOfFiles = 0) {
    let dir = './output';
    let data = 'Hello World!\n';
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);``
    }
    const fileNames = getRandomStringArray(numberOfFiles, 6);
    fileNames.forEach((file) => {
        let filePath = dir + '/' + file + '.json';
        awaitDelete(filePath);
    })
}

module.exports = { createAndDeleteRandomJson }